package task5;

public class Time {
    private int hours;
    private int minutes;
    private int seconds;

    public Time() {
        this(0, 0, 0);
    }

    public Time(int hours, int minutes, int seconds) {
        setTime(hours, minutes, seconds);
    }

    public void setTime(int hours, int minutes, int seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
        validateTime();
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
        validateHours();
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
        validateMinutes();
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
        validateSeconds();
    }

    @Override
    public String toString() {
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    private void validateTime() {
        validateHours();
        validateMinutes();
        validateSeconds();
    }

    private void validateHours() {
        if (hours < 0 || hours > 23) {
            hours = 0;
        }
    }

    private void validateMinutes() {
        if (minutes < 0 || minutes > 59) {
            minutes = 0;
        }
    }

    private void validateSeconds() {
        if (seconds < 0 || seconds > 59) {
            seconds = 0;
        }
    }
}
