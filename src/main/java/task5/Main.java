package task5;

public class Main {

    public static void main(String[] args) {
        Time time = new Time();
        System.out.println("Time initialized by default constructor.");
        System.out.println("\t" + time);

        System.out.println("Set hours=16, minutes=37, seconds=59");
        time.setTime(16, 37, 59);
        System.out.println("\t" + time);

        System.out.println("Change minutes=45, seconds=07");
        time.setMinutes(45);
        time.setSeconds(7);
        System.out.println("\t" + time);

        System.out.println("Try to set invalid time. hours=77, minutes=77, seconds=77");
        time.setTime(77, 77, 77);
        System.out.println("\t" + time);
    }
}
