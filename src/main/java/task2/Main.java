package task2;

public class Main {

    public static void main(String[] args) {
        Test2 test = new Test2();
        printVariables("Variables initialized with default constructor.\n", test);

        test.setFirstVariable(111);
        test.setSecondVariable(777);
        printVariables("Variables initialized with setters.\n", test);

        Test2 test2 = new Test2(10, 20);
        printVariables("Variables initialized with constructor.\n", test2);
    }

    private static void printVariables(String description, Test2 test) {
        System.out.printf(description +
                "\tfirstVariable: %d\n" +
                "\tsecondVariable: %d\n", test.getFirstVariable(), test.getSecondVariable());
    }
}
