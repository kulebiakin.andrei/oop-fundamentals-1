package task4;

public class DecimalCounter {
    private final int min;
    private final int max;
    private int current;

    /**
     * Initializes default values.
     * min = 0,
     * max = 10.
     */
    public DecimalCounter() {
        this(0, 10, 0);
    }

    /**
     * @param min The start of the cycle (inclusive)
     * @param max The end of the cycle (inclusive)
     */
    public DecimalCounter(int min, int max, int current) {
        this.min = min;
        this.max = max;
        this.current = current;
    }

    public void increase() {
        current++;
        checkBorderValues();
    }

    public void decrease() {
        current--;
        checkBorderValues();
    }

    public int getCurrent() {
        return current;
    }

    private void checkBorderValues() {
        if (current < min) {
            current = max;
        } else if (current > max) {
            current = min;
        }
    }
}
