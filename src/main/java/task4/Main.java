package task4;

public class Main {

    public static void main(String[] args) {
        // Test Decimal Counter with default min and max values
        DecimalCounter decimalCounter = new DecimalCounter();
        System.out.println("Test Decimal Counter with default min, max, and current values");
        testDecimalCounter(decimalCounter);

        System.out.println();

        // Test Decimal Counter with default min and max values
        DecimalCounter decimalCounterArbitrary = new DecimalCounter(20, 23, 20);
        System.out.println("Test Decimal Counter with arbitrary values. min = 20, max = 23, current = 20");
        testDecimalCounter(decimalCounterArbitrary);
    }

    private static void testDecimalCounter(DecimalCounter decimalCounter) {
        System.out.println("Initial current: " + decimalCounter.getCurrent());
        System.out.println("Test increasing 5 times:");
        for (int i = 0; i < 5; i++) {
            decimalCounter.increase();
            System.out.println("\tCurrent: " + decimalCounter.getCurrent());
        }

        System.out.println("Test decreasing 7 times:");
        for (int i = 0; i < 7; i++) {
            decimalCounter.decrease();
            System.out.println("\tCurrent: " + decimalCounter.getCurrent());
        }
    }
}
