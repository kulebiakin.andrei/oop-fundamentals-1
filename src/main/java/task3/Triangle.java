package task3;

class Triangle {
    private final double firstSide;
    private final double secondSide;
    private final double thirdSide;

    public Triangle(double firstSide, double secondSide, double thirdSide) {
        this.firstSide = firstSide;
        this.secondSide = secondSide;
        this.thirdSide = thirdSide;

        validateTriangle();
    }

    /**
     * Uses Heron's formula Area=sqrt(s(s-a)(s-b)(s-c)), where s is the semi-perimeter, s=(a+b+c)/2.
     */
    public double getArea() {
        double s = getPerimeter() / 2;
        return Math.sqrt(s * (s - firstSide) * (s - secondSide) * (s - thirdSide));
    }

    public double getPerimeter() {
        return firstSide + secondSide + thirdSide;
    }

    private void validateTriangle() {
        if (firstSide + secondSide < thirdSide || firstSide + thirdSide < secondSide || secondSide + thirdSide < firstSide) {
            throw new IllegalArgumentException("Invalid sides for a triangle");
        }
    }
}

