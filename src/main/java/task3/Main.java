package task3;

public class Main {

    public static void main(String[] args) {
        Triangle triangle = new Triangle(30, 40, 50);
        double perimeter = triangle.getPerimeter();
        double area = triangle.getArea();

        System.out.println("perimeter: " + perimeter);
        System.out.println("area: " + area);
    }
}
