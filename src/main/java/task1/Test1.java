package task1;

public class Test1 {
    private int firstVariable;
    private int secondVariable;

    public int getFirstVariable() {
        return firstVariable;
    }

    public void setFirstVariable(int firstVariable) {
        this.firstVariable = firstVariable;
    }

    public int getSecondVariable() {
        return secondVariable;
    }

    public void setSecondVariable(int secondVariable) {
        this.secondVariable = secondVariable;
    }

    public int getSumOfVariables() {
        return firstVariable + secondVariable;
    }

    public int getLargestVariable() {
        return Math.max(firstVariable, secondVariable);
    }
}
