package task1;

public class Main {

    public static void main(String[] args) {
        Test1 test1 = new Test1();
        test1.setFirstVariable(42);
        test1.setSecondVariable(37);

        System.out.println("Sum: " + test1.getSumOfVariables());
        System.out.println("Largest variable: " + test1.getLargestVariable());
    }
}
